package pl.makzyt.inventory_book.connector;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import pl.makzyt.inventory_book.util.URLUtils;

import java.util.HashMap;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ServerConnector {
    private static final String contentType = "application/json";
    private static final String className = ServerConnector.class.getName();
    private static final Logger logger = Logger.getLogger(className);
    private String url;

    public String responseFromHttpGet() {
        HttpClient client = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost(url);
            request.addHeader("accept", contentType);

            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();

            return EntityUtils.toString(entity);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    public String responseFromHttpPost(HashMap<String, Object> params,
                                           String input) {

        HttpClient client = HttpClientBuilder.create().build();
        String urlWithParams = params != null
            ? URLUtils.addParamToURL(url, params)
            : url;

        try {
            StringEntity entity = new StringEntity(input);
            HttpPost request = new HttpPost(urlWithParams);
            request.addHeader("content-type", contentType);
            request.setEntity(entity);

            HttpResponse response = client.execute(request);
            HttpEntity responseEntity = response.getEntity();

            return EntityUtils.toString(responseEntity);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return null;
    }
}
