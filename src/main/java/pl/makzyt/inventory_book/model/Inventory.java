package pl.makzyt.inventory_book.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "inventory")
public class Inventory {
    @Id
    private String id;

    @Column(name = "is_available")
    private Boolean isAvailable;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "income_date")
    private Date incomeDate;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "unit_price")
    private Double unitPrice;

    @Column(name = "income_number")
    private Integer incomeNumber;

    @Column(name = "outcome_number")
    private Integer outcomeNumber;

    @Column(name = "income_price")
    private Double incomePrice;

    @Column(name = "outcome_price")
    private Double outcomePrice;

    @ManyToOne
    private Cell cell;

    @ManyToOne
    private Employee owner;
}
