package pl.makzyt.inventory_book.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@Table(name = "remembered_user")
public class RememberedUser {
    @Id
    private String id;

    @ManyToOne
    private Employee user;
}
