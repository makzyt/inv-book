package pl.makzyt.inventory_book.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "cell")
public class Cell {
    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    private Cell parentCell;
}
