package pl.makzyt.inventory_book.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    private Long id;

    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "degree")
    private String degree;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "workplace", nullable = false)
    private String workplace;

    @Column(name = "function")
    private String function;

    @ManyToOne
    private Room room;

    @ManyToOne
    private Cell cell;

    @Column(name = "is_manager", nullable = false)
    private Boolean isManager;

    @Column(name = "is_asi", nullable = false)
    private Boolean isAsi;

    public String fullName() {
        String degree = getDegree() != null ? getDegree() : "";
        String name = getName() != null ? getName() : "";

        return name.equals("") ? getLogin() : degree + " " + name;
    }
}
