package pl.makzyt.inventory_book.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "room")
public class Room {
    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private RoomType type;

    @Column(name = "campus")
    private String campus;

    @Column(name = "building")
    private String building;

    @ManyToOne
    private Cell cell;

    public enum RoomType {
        OFFICE,
        PRACTICE_ROOM
    }
}
