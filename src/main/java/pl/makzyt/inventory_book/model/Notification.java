package pl.makzyt.inventory_book.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@AllArgsConstructor
@Data
@Entity
@NoArgsConstructor
@Table(name = "notification")
public class Notification {
    @GeneratedValue
    @Id
    private Long id;

    @Column(name = "content")
    private String content;

    @ManyToOne
    private Employee recipient;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type")
    private NotificationType type;

    @Column(name = "time_sent")
    private Timestamp timeSent;

    public enum NotificationType {
        ASSIGNED_TO_ITEM
    }
}
