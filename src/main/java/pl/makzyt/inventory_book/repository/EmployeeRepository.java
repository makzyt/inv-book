package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.Employee;

import java.util.Set;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findById(Long id);
    Set<Employee> findAllByOrderByName();
}
