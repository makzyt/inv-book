package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.ITSystem;

@Repository
public interface ITSystemRepository extends JpaRepository<ITSystem, Long> {
    ITSystem findById(String id);
}
