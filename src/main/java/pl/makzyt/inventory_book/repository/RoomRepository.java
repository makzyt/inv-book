package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, String> {
    Room findById(String id);
}
