package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.Notification;

import java.util.List;

@Repository
public interface NotificationRepository
    extends JpaRepository<Notification, Long> {

    Notification findById(Long id);
    List<Notification> findAllByOrderByTimeSentDesc();
}
