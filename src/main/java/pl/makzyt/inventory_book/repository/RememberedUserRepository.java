package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.RememberedUser;

@Repository
public interface RememberedUserRepository
    extends JpaRepository<RememberedUser, Long> {

    RememberedUser findById(String id);
}
