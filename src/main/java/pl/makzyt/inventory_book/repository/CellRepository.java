package pl.makzyt.inventory_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.makzyt.inventory_book.model.Cell;

import java.util.Set;

@Repository
public interface CellRepository extends JpaRepository<Cell, Long> {
    Cell findById(Long id);
    Set<Cell> findAllByOrderByName();
}
