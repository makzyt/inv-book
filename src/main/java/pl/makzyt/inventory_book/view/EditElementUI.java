package pl.makzyt.inventory_book.view;

import com.vaadin.data.Binder;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Inventory;
import pl.makzyt.inventory_book.model.Notification;
import pl.makzyt.inventory_book.repository.CellRepository;
import pl.makzyt.inventory_book.repository.EmployeeRepository;
import pl.makzyt.inventory_book.repository.InventoryRepository;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.service.NotificationService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

import java.util.Set;

@SpringUI(path = RouteUtils.URL_UNIT_EDIT)
public class EditElementUI extends UI {
    private Label nameLabel = new Label();
    private FormLayout form = new FormLayout();
    private Grid<Inventory> grid = new Grid<>();
    private Binder<Inventory> binder = new Binder<>();
    private Inventory inventory = new Inventory();

    @Autowired
    private AuthService authService;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CellRepository cellRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    protected void init(VaadinRequest request) {
        Employee authEmp = authService.authenticateUser();

        VerticalLayout layout = new VerticalLayout();

        NativeSelect<Employee> ownerField = new NativeSelect<>();
        NativeSelect<Cell> cellField = new NativeSelect<>();
        Button saveButton = new Button("Zapisz");

        ownerField.setItems(employeeRepository.findAllByOrderByName());
        ownerField.setItemCaptionGenerator(Employee::fullName);

        cellField.setItems(cellRepository.findAllByOrderByName());
        cellField.setItemCaptionGenerator(Cell::getName);

        form.addComponent(nameLabel);
        form.addComponent(ownerField);
        form.addComponent(cellField);
        form.addComponent(saveButton);
        form.setVisible(false);

        layout.addComponent(form);
        layout.addComponent(grid);

        if (authEmp != null) {
            if (authEmp.getIsManager() || authEmp.getIsAsi()) {
                Set<Inventory> items
                    = inventoryRepository.findAllByOrderById();

                grid.setSizeFull();
                grid.addColumn(Inventory::getName)
                    .setCaption("Nazwa");
                grid.addColumn(Inventory::getDescription)
                    .setCaption("Opis");
                grid.addColumn(i -> {
                    Employee owner = i.getOwner();

                    if (owner == null) {
                        return "";
                    }

                    return owner.fullName();
                }).setCaption("Właściciel");
                grid.addColumn(i -> {
                    Cell cell = i.getCell();

                    if (cell == null) {
                        return "";
                    }

                    return cell.getName();
                }).setCaption("Jednostka");
                grid.setItems(items);
                grid.addSelectionListener(s -> updateForm());

                saveButton.addClickListener(s -> saveElement());

                binder.setBean(inventory);
                binder.forField(ownerField)
                    .bind(Inventory::getOwner, Inventory::setOwner);
                binder.forField(cellField)
                    .bind(Inventory::getCell, Inventory::setCell);

                setContent(new ApplicationLayout(layout, this, authService));
            } else {
                RouteUtils.route(this, RouteUtils.URL_LOGIN);
            }
        }
    }

    private void saveElement() {
        grid.setItems(inventoryRepository.findAllByOrderById());

        Employee authEmp = authService.authenticateUser();

        String name = inventory.getName() == null ? "" : inventory.getName();
        String desc = inventory.getDescription() == null
            ? ""
            : " (" + inventory.getDescription() + ")";

        String notificationContent = String.format(
            "Użytkownik %s przydzielił tobie przedmiot %s%s [%s].",
            authEmp.getName(), name, desc, inventory.getId());

        Long ownerId = inventory.getOwner().getId();
        Employee owner = employeeRepository.findById(ownerId);

        notificationService.sendNotification(
            owner, notificationContent,
            Notification.NotificationType.ASSIGNED_TO_ITEM);

        inventoryRepository.save(inventory);
    }

    private void updateForm() {
        if (grid.asSingleSelect().isEmpty()) {
            form.setVisible(false);
            return;
        }

        inventory = grid.asSingleSelect().getValue();
        nameLabel.setValue(inventory.getName());
        binder.setBean(inventory);
        form.setVisible(true);
    }
}
