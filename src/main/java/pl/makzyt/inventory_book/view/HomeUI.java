package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

@SpringUI(path = RouteUtils.URL_HOME)
public class HomeUI extends UI {
    private VerticalLayout layout;
    private Label greeting;

    @Autowired
    private AuthService authService;

    @Override
    protected void init(VaadinRequest request) {
        layout = new VerticalLayout();
        greeting = new Label("", ContentMode.HTML);

        layout.addComponent(greeting);

        Employee authEmp = authService.authenticateUser();

        if (authEmp != null) {
            greeting.setValue("Witaj, <b>" + authEmp.fullName() + "</b>!");
            setContent(new ApplicationLayout(layout, this, authService));
        } else {
            RouteUtils.route(this, RouteUtils.URL_LOGIN);
        }
    }
}
