package pl.makzyt.inventory_book.view.layout;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.apache.log4j.Logger;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;

public class ApplicationLayout extends VerticalLayout {
    private static String className = ApplicationLayout.class.getName();
    private static Logger logger = Logger.getLogger(className);
    private MenuBar menu = new MenuBar();
    private UI ui;

    private AuthService authService;

    public ApplicationLayout(Layout innerLayout, UI ui,
                             AuthService authService) {
        this.authService = authService;
        this.ui = ui;

        Employee authEmp = authService.authenticateUser();

        addComponent(menu);
        addComponent(innerLayout);

        menu.setWidth(100.f, Sizeable.Unit.PERCENTAGE);

        menu.addItem("Strona główna",
            s -> RouteUtils.route(ui, RouteUtils.URL_HOME));

        MenuBar.MenuItem managementItem = menu.addItem("Zarządzanie", null);
        managementItem.addItem("Przypisany majątek",
            s -> RouteUtils.route(ui, RouteUtils.URL_ASSIGNED_PROPERTY));
        managementItem.addItem("Wyszukaj element",
            s -> RouteUtils.route(ui, RouteUtils.URL_SEARCH_ELEMENT));

        if (authEmp.getIsManager() || authEmp.getIsAsi()) {
            managementItem.addSeparator();
            managementItem.addItem("Majątek zarządzanej jednostki",
                s -> RouteUtils.route(ui, RouteUtils.URL_UNIT_PROPERTY));

            managementItem.addItem("Edycja elementu",
                s -> RouteUtils.route(ui, RouteUtils.URL_UNIT_EDIT));
        }

        menu.addItem("Powiadomienia",
            s -> RouteUtils.route(ui, RouteUtils.URL_USER_NOTIFICATIONS));

        menu.addItem("Wyloguj się", s -> {
            authService.logoutUser();
            logger.info("User " + authEmp.getLogin() + " logged out");
            RouteUtils.route(ui, RouteUtils.URL_LOGIN);
        });
    }
}
