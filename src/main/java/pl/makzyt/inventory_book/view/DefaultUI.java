package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import pl.makzyt.inventory_book.util.RouteUtils;

@SpringUI(path = RouteUtils.URL_DEFAULT)
public class DefaultUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        RouteUtils.route(this, RouteUtils.URL_HOME);
    }
}
