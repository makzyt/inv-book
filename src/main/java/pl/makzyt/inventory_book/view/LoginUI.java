package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.service.ERPService;
import pl.makzyt.inventory_book.util.RouteUtils;

@SpringUI(path = RouteUtils.URL_LOGIN)
public class LoginUI extends UI {
    private static String className = LoginUI.class.getName();
    private static Logger logger = Logger.getLogger(className);
    private VerticalLayout layout;
    private Panel panel;
    private FormLayout form;
    private TextField usernameField;
    private TextField passwordField;
    private Button submitButton;
    private CheckBox rememberCheckBox;
    private Label infoLabel;

    @Autowired
    private ERPService erpService;

    @Autowired
    private AuthService authService;

    @Override
    protected void init(VaadinRequest request) {
        layout = new VerticalLayout();
        panel = new Panel("Zaloguj się");
        form = new FormLayout();
        usernameField = new TextField("Nazwa użytkownika");
        passwordField = new PasswordField("Hasło");
        submitButton = new Button("Zaloguj się");
        rememberCheckBox = new CheckBox("Zapamiętaj mnie");
        infoLabel = new Label();

        layout.setSizeFull();
        layout.addComponent(panel);
        layout.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        submitButton.addClickListener(e -> {
            String login = usernameField.getValue();
            String password = passwordField.getValue();
            boolean remember = rememberCheckBox.getValue();

            Employee emp = authService.loginUser(login, password, remember);

            if (emp != null) {
                infoLabel.setValue("Logowanie...");
                logger.info("User " + emp.getLogin() + " logged in");
                RouteUtils.route(this, RouteUtils.URL_HOME);
            } else {
                infoLabel.setValue("Nieprawidłowe dane");
            }
        });

        form.setMargin(true);
        form.addComponent(usernameField);
        form.addComponent(passwordField);
        form.addComponent(submitButton);
        form.addComponent(rememberCheckBox);
        form.addComponent(infoLabel);

        panel.setSizeUndefined();
        panel.setContent(form);

        Employee authEmp = authService.authenticateUser();

        if (authEmp != null) {
            RouteUtils.route(this, RouteUtils.URL_HOME);
        }

        setContent(layout);
    }
}
