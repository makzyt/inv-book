package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Notification;
import pl.makzyt.inventory_book.repository.NotificationRepository;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

import java.util.List;
import java.util.stream.Collectors;

@SpringUI(path = RouteUtils.URL_USER_NOTIFICATIONS)
public class NotificationUI extends UI {
    private VerticalLayout layout;
    private FormLayout form;

    @Autowired
    private AuthService authService;

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    protected void init(VaadinRequest request) {
        layout = new VerticalLayout();
        form = new FormLayout();

        layout.addComponent(form);

        Employee authEmp = authService.authenticateUser();

        if (authEmp != null) {
            List<Notification> notifications =
                notificationRepository.findAllByOrderByTimeSentDesc()
                .stream()
                .filter(n -> {
                    Employee recipient = n.getRecipient();

                    return recipient != null
                        && recipient.getId().equals(authEmp.getId());
                }).collect(Collectors.toList());

            for (Notification n : notifications) {
                Label label = new Label();
                label.setWidth("100%");
                label.setCaption(n.getTimeSent().toString());
                label.setValue(n.getContent());
                form.addComponent(label);
            }

            setContent(new ApplicationLayout(layout, this, authService));
        } else {
            RouteUtils.route(this, RouteUtils.URL_LOGIN);
        }
    }
}
