package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Inventory;
import pl.makzyt.inventory_book.repository.InventoryRepository;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

import java.util.List;
import java.util.stream.Collectors;

@SpringUI(path = RouteUtils.URL_UNIT_PROPERTY)
public class UnitPropertyUI extends UI {
    @Autowired
    private AuthService authService;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    protected void init(VaadinRequest request) {
        Employee authEmp = authService.authenticateUser();

        VerticalLayout layout = new VerticalLayout();
        Label summary = new Label("", ContentMode.HTML);
        Grid<Inventory> grid = new Grid<>();

        layout.addComponent(summary);

        if (authEmp != null && authEmp.getIsManager()) {
            if (authEmp.getCell() == null) {
                summary.setValue("Jesteś kierownikiem, ale nie przypisano "
                    + "Tobie żadnej jednostki.");
            } else {
                layout.addComponent(grid);
                List<Inventory> property = inventoryRepository.findAll()
                    .stream()
                    .filter(i -> {
                        Cell authEmpCell = authEmp.getCell();
                        Cell invCell = i.getCell();

                        return authEmpCell != null
                            && invCell != null
                            && authEmpCell.getId().equals(invCell.getId());
                    }).collect(Collectors.toList());

                grid.setSizeFull();
                grid.addColumn(Inventory::getName)
                    .setCaption("Nazwa");
                grid.addColumn(Inventory::getDescription)
                    .setCaption("Opis");
                grid.addColumn(Inventory::getIncomeDate)
                    .setCaption("Data zapisu");
                grid.addColumn(Inventory::getUnitPrice)
                    .setCaption("Wartość");
                grid.setItems(property);

                double priceSum = property.stream()
                    .mapToDouble(Inventory::getUnitPrice)
                    .sum();

                summary.setValue(String.format(
                    "Jednostka: <b>%s</b><br>"
                        + "Właściciel jednostki: <b>%s</b><br>"
                        + "Wartość majątku jednostki: <b>%.2f zł</b>",
                    authEmp.getCell().getName(),
                    authEmp.fullName(),
                    priceSum));
            }

            setContent(new ApplicationLayout(layout, this, authService));
        } else {
            RouteUtils.route(this, RouteUtils.URL_LOGIN);
        }
    }
}
