package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Inventory;
import pl.makzyt.inventory_book.repository.InventoryRepository;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

import java.util.List;
import java.util.stream.Collectors;

@SpringUI(path = RouteUtils.URL_ASSIGNED_PROPERTY)
public class AssignedPropertyUI extends UI {
    @Autowired
    private AuthService authService;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    protected void init(VaadinRequest request) {
        Employee authEmp = authService.authenticateUser();

        VerticalLayout layout = new VerticalLayout();
        Label summary = new Label("", ContentMode.HTML);
        Grid<Inventory> grid = new Grid<>();

        layout.addComponent(summary);
        layout.addComponent(grid);

        if (authEmp != null) {
            List<Inventory> property = inventoryRepository.findAll().stream()
                .filter(i -> {
                    Employee owner = i.getOwner();
                    Long authId = authEmp.getId();

                    return (owner != null) && (owner.getId().equals(authId));
                }).collect(Collectors.toList());

            grid.setSizeFull();
            grid.addColumn(Inventory::getName).setCaption("Nazwa");
            grid.addColumn(Inventory::getDescription).setCaption("Opis");
            grid.addColumn(Inventory::getIncomeDate).setCaption("Data zapisu");
            grid.addColumn(Inventory::getUnitPrice).setCaption("Wartość");
            grid.setItems(property);

            double priceSum = property.stream()
                .mapToDouble(Inventory::getUnitPrice)
                .sum();

            summary.setValue(String.format(
                "Właściciel majątku: <b>%s</b><br>"
                + "Wartość majątku: <b>%.2f zł</b>",
                authEmp.getName(), priceSum));

            setContent(new ApplicationLayout(layout, this, authService));
        } else {
            RouteUtils.route(this, RouteUtils.URL_LOGIN);
        }
    }
}
