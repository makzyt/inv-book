package pl.makzyt.inventory_book.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Inventory;
import pl.makzyt.inventory_book.repository.InventoryRepository;
import pl.makzyt.inventory_book.service.AuthService;
import pl.makzyt.inventory_book.util.RouteUtils;
import pl.makzyt.inventory_book.view.layout.ApplicationLayout;

import java.util.List;
import java.util.stream.Collectors;

@SpringUI(path = RouteUtils.URL_SEARCH_ELEMENT)
public class SearchElementUI extends UI {
    @Autowired
    private AuthService authService;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    protected void init(VaadinRequest request) {
        Employee authEmp = authService.authenticateUser();

        VerticalLayout layout = new VerticalLayout();
        TextField searchField = new TextField("Podaj numer ewidencyjny");
        Grid<Inventory> grid = new Grid<>();

        layout.addComponent(searchField);
        layout.addComponent(grid);

        if (authEmp != null) {

            grid.setSizeFull();
            grid.addColumn(Inventory::getId)
                .setCaption("Numer ewidencyjny");
            grid.addColumn(Inventory::getName)
                .setCaption("Nazwa");
            grid.addColumn(Inventory::getDescription)
                .setCaption("Opis");
            grid.addColumn(Inventory::getIncomeDate)
                .setCaption("Data zapisu");
            grid.addColumn(Inventory::getUnitPrice)
                .setCaption("Wartość");
            grid.addColumn(i -> {
                Employee owner = i.getOwner();

                if (owner == null) {
                    return "";
                }

                return i.getOwner().fullName();
            }).setCaption("Właściciel");

            searchField.addValueChangeListener(s -> {
                List<Inventory> property = inventoryRepository.findAll()
                    .stream()
                    .filter(i ->
                        i.getId()
                            .toLowerCase()
                            .contains(s.getValue().toLowerCase()))
                    .collect(Collectors.toList());
                grid.setItems(property);
            });

            setContent(new ApplicationLayout(layout, this, authService));
        } else {
            RouteUtils.route(this, RouteUtils.URL_LOGIN);
        }
    }
}
