package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.model.Room;
import pl.makzyt.inventory_book.repository.CellRepository;

@Service
public class RoomService {
    @Autowired
    private CellRepository cellRepository;

    public Room loadFromJson(JSONObject object) {
        Long cellId = object.isNull("cellId")
            ? null
            : object.getLong("cellId");
        Cell cell = cellRepository.findById(cellId);
        String type = object.getString("roomType");

        Room r = new Room();
        r.setId(object.getString("roomId"));
        r.setCampus(object.getString("campus"));
        r.setBuilding(object.getString("building"));
        r.setName(object.getString("roomName"));
        r.setCell(cell);

        if (type.equals("Pomieszczenie biurowe")) {
            r.setType(Room.RoomType.OFFICE);
        } else {
            r.setType(Room.RoomType.PRACTICE_ROOM);
        }

        return r;
    }
}
