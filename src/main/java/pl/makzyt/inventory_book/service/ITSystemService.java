package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.ITSystem;
import pl.makzyt.inventory_book.repository.EmployeeRepository;

@Service
public class ITSystemService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public ITSystem loadFromJson(JSONObject object) {
        Long adminId = object.isNull("administratorId")
            ? null
            : object.getLong("administratorId");

        ITSystem its = new ITSystem();
        its.setId(object.getString("id"));
        its.setName(object.getString("name"));
        its.setDescription(object.getString("description"));
        its.setAdministrator(employeeRepository.findById(adminId));

        return its;
    }
}
