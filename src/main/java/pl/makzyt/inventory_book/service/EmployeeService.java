package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.model.Room;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.repository.CellRepository;
import pl.makzyt.inventory_book.repository.RoomRepository;

@Service
public class EmployeeService {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private CellRepository cellRepository;

    public Employee loadFromJson(JSONObject object) {
        Employee e = new Employee();
        Room room = roomRepository.findById(object.getString("roomId"));
        Cell cell = cellRepository.findById(object.getLong("cellId"));
        String function = object.isNull("function")
            ? null
            : object.getString("function");

        e.setId(object.getLong("employeeId"));
        e.setLogin(object.getString("login"));
        e.setName(object.getString("name"));
        e.setEmail(object.getString("email"));
        e.setDegree(object.getString("degree"));
        e.setIsAsi(object.getBoolean("isAsi"));
        e.setIsManager(object.getBoolean("isManager"));
        e.setWorkplace(object.getString("workplace"));
        e.setCell(cell);
        e.setFunction(function);
        e.setRoom(room);

        return e;
    }
}
