package pl.makzyt.inventory_book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.RememberedUser;
import pl.makzyt.inventory_book.repository.EmployeeRepository;
import pl.makzyt.inventory_book.repository.RememberedUserRepository;
import pl.makzyt.inventory_book.util.CookieUtils;

import javax.servlet.http.Cookie;

@Service
public class AuthService {
    @Autowired
    private ERPService erpService;

    @Autowired
    private RememberedUserService rememberedUserService;

    @Autowired
    private RememberedUserRepository rememberedUserRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee loginUser(String login, String password, boolean remember) {
        int lifetime = remember
            ? CookieUtils.MAX_COOKIE_AGE_REMEMBERED
            : CookieUtils.MAX_COOKIE_AGE_NOT_REMEMBERED;
        Employee e = erpService.checkLogin(login, password);

        if (e == null) {
            return null;
        }

        String sessionId = rememberedUserService.rememberUser(e);
        Cookie cookie = new Cookie(CookieUtils.SESSION_COOKIE, sessionId);

        CookieUtils.addCookie(cookie, lifetime);
        return e;
    }

    public void logoutUser() {
        Cookie cookie = CookieUtils.getCookieByName(CookieUtils.SESSION_COOKIE);
        String userId = cookie.getValue();
        RememberedUser remUser = rememberedUserRepository.findById(userId);

        rememberedUserRepository.delete(remUser);
        CookieUtils.eraseCookie(CookieUtils.SESSION_COOKIE);
    }

    public Employee authenticateUser() {
        Cookie cookie = CookieUtils.getCookieByName(CookieUtils.SESSION_COOKIE);

        if (cookie == null) {
            return null;
        }

        String userId = cookie.getValue();
        RememberedUser remUser = rememberedUserRepository.findById(userId);

        return remUser != null ? remUser.getUser() : null;
        //return employeeRepository.findById(250L);
    }
}
