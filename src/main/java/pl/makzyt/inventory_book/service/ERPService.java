package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.connector.ServerConnector;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.util.RouteUtils;

import java.util.HashMap;

@Service
public class ERPService {
    @Autowired
    private EmployeeService employeeService;

    public Employee checkLogin(String login, String password) {
        String url = RouteUtils.EXT_URL_REST_CHECKLOGIN;
        ServerConnector connector = new ServerConnector(url);
        Employee e = null;

        HashMap<String, Object> params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);

        String response = connector.responseFromHttpPost(params, "");
        JSONObject jsonObject = new JSONObject(response);

        if (jsonObject.getBoolean("success")) {
            JSONObject details = jsonObject.getJSONObject("details");
            e = employeeService.loadFromJson(details);
        }

        return e;
    }
}
