package pl.makzyt.inventory_book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Notification;
import pl.makzyt.inventory_book.repository.NotificationRepository;

import java.sql.Timestamp;

@Service
public class NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    public void sendNotification(Employee emp, String content,
                                 Notification.NotificationType type) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Notification notification = new Notification();
        notification.setTimeSent(timestamp);
        notification.setContent(content);
        notification.setType(type);
        notification.setRecipient(emp);

        notificationRepository.save(notification);
    }
}
