package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.repository.CellRepository;

@Service
public class CellService {
    @Autowired
    private CellRepository cellRepository;

    public Cell loadFromJson(JSONObject object) {
        Long parentId = object.isNull("overridingCellId")
            ? null
            : object.getLong("overridingCellId");

        Cell c = new Cell();
        Cell parent = cellRepository.findById(parentId);

        c.setId(object.getLong("cellId"));
        c.setName(object.getString("cellName"));
        c.setParentCell(parent);

        return c;
    }
}
