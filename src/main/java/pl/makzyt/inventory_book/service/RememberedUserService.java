package pl.makzyt.inventory_book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.RememberedUser;
import pl.makzyt.inventory_book.repository.RememberedUserRepository;
import pl.makzyt.inventory_book.util.MessageDigester;

import java.math.BigInteger;
import java.security.SecureRandom;

@Service
public class RememberedUserService {
    private SecureRandom random = new SecureRandom();

    @Autowired
    private RememberedUserRepository rememberedUserRepository;

    public String rememberUser(Employee e) {
        BigInteger number = new BigInteger(128, random);
        String id = MessageDigester.digestSHA256(number.toString());

        String sessionId = e.getLogin() + "_" + id;

        RememberedUser user = new RememberedUser();
        user.setId(sessionId);
        user.setUser(e);

        rememberedUserRepository.save(user);

        return sessionId;
    }
}
