package pl.makzyt.inventory_book.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.makzyt.inventory_book.model.Cell;
import pl.makzyt.inventory_book.model.Employee;
import pl.makzyt.inventory_book.model.Inventory;
import pl.makzyt.inventory_book.repository.CellRepository;
import pl.makzyt.inventory_book.repository.EmployeeRepository;
import pl.makzyt.inventory_book.util.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

@Service
public class InventoryService {
    @Autowired
    private CellRepository cellRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public Inventory loadFromJson(JSONObject object) {
        DateFormat format = TextUtils.dateFormat;

        String description = object.isNull("description")
            ? null
            : object.getString("description");
        Long cellId = object.isNull("cellId")
            ? null
            : object.getLong("cellId");
        Long employeeId = object.isNull("employeeId")
            ? null
            : object.getLong("employeeId");
        Integer incomeNumber = object.isNull("incomeNumber")
            ? null
            : object.getInt("incomeNumber");
        Integer outcomeNumber = object.isNull("outcomeNumber")
            ? null
            : object.getInt("outcomeNumber");
        Double incomePrice = object.isNull("incomePrice")
            ? null
            : object.getDouble("incomePrice");
        Double outcomePrice = object.isNull("outcomePrice")
            ? null
            : object.getDouble("outcomePrice");

        Cell cell = cellRepository.findById(cellId);
        Employee employee = employeeRepository.findById(employeeId);

        Inventory inv = new Inventory();
        inv.setId(object.getString("inventoryNumber"));
        inv.setIsAvailable(object.getString("status").equals("na stanie"));
        inv.setName(object.getString("name"));
        inv.setDescription(description);
        inv.setVatNumber(object.getString("vatNumber"));
        inv.setUnitPrice(object.getDouble("unitPrice"));
        inv.setIncomeNumber(incomeNumber);
        inv.setOutcomeNumber(outcomeNumber);
        inv.setIncomePrice(incomePrice);
        inv.setOutcomePrice(outcomePrice);
        inv.setCell(cell);
        inv.setOwner(employee);

        try {
            Date parsed = format.parse(object.getString("incomeDate"));
            inv.setIncomeDate(new java.sql.Date(parsed.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
            return inv;
        }

        return inv;
    }
}
