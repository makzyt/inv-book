package pl.makzyt.inventory_book.util;

import lombok.experimental.UtilityClass;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

@UtilityClass
public class URLUtils {
    private static String className = URLUtils.class.getName();
    private static Logger logger = Logger.getLogger(className);

    public String addParamToURL(String url, HashMap<String, Object> params) {
        StringBuilder path = new StringBuilder(url);
        int counter = 0;

        if (params.size() == 0) {
            return url;
        }

        path.append("?");

        for (String key : params.keySet()) {
            String value = params.get(key).toString();

            try {
                path.append(URLEncoder.encode(key, "UTF-8"));
                path.append("=");
                path.append(URLEncoder.encode(value, "UTF-8"));

                if (counter < params.size() - 1) {
                    path.append("&");
                }
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage());
            }

            counter++;
        }

        return path.toString();
    }
}
