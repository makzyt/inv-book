package pl.makzyt.inventory_book.util;

import lombok.experimental.UtilityClass;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@UtilityClass
public class TextUtils {
    public final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}
