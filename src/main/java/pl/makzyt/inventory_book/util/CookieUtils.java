package pl.makzyt.inventory_book.util;

import com.vaadin.server.VaadinService;
import lombok.experimental.UtilityClass;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public class CookieUtils {
    public final String SESSION_COOKIE = "session-id";
    public final String SESSION_COOKIE_PATH = "/";
    public final int MAX_COOKIE_AGE_REMEMBERED = 60 * 60 * 24 * 7;
    public final int MAX_COOKIE_AGE_NOT_REMEMBERED = 60 * 60 * 24;

    public void addCookie(Cookie cookie, int age) {
        cookie.setMaxAge(age);
        cookie.setPath(SESSION_COOKIE_PATH);
        VaadinService.getCurrentResponse().addCookie(cookie);
    }

    public List<Cookie> fetchCookies() {
        return Arrays.asList(VaadinService.getCurrentRequest().getCookies());
    }

    public Cookie getCookieByName(String name) {
        return fetchCookies()
            .stream()
            .filter(cookie -> cookie.getName().equals(name))
            .findFirst()
            .orElse(null);
    }

    public void eraseCookie(String cookieName) {
        Cookie cookie = getCookieByName(cookieName);

        if (cookie == null) {
            return;
        }

        cookie.setValue("");
        cookie.setMaxAge(0);
        cookie.setPath(SESSION_COOKIE_PATH);
        VaadinService.getCurrentResponse().addCookie(cookie);
    }
}

