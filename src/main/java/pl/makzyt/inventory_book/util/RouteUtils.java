package pl.makzyt.inventory_book.util;

import com.vaadin.ui.UI;
import lombok.experimental.UtilityClass;

@UtilityClass
public class RouteUtils {
    public final String EXT_URL_BASE
        = "http://212.122.192.216:8097";

    public final String EXT_URL_REST_CHECKLOGIN
        = EXT_URL_BASE + "/api/v1/user/checklogin";

    public final String EXT_URL_REST_CELL_ALL
        = EXT_URL_BASE + "/api/v1/cell/all";

    public final String EXT_URL_REST_EMAIL_SEND
        = EXT_URL_BASE + "/api/v1/email/send";

    public final String EXT_URL_REST_EMPLOYEE_ALL
        = EXT_URL_BASE + "/api/v1/employee/all";

    public final String EXT_URL_REST_ITSYSTEM_ALL
        = EXT_URL_BASE + "/api/v1/itsystem/all";

    public final String EXT_URL_REST_INVENTORY_ALL
        = EXT_URL_BASE + "/api/v1/inventory/all";

    public final String EXT_URL_REST_ROOM_ALL
        = EXT_URL_BASE + "/api/v1/room/all";

    public final String URL_DEFAULT = "/";
    public final String URL_HOME = "/home";
    public final String URL_LOGIN = "/login";
    public final String URL_ASSIGNED_PROPERTY = "/user/assigned_property";
    public final String URL_SEARCH_ELEMENT = "/user/search_element";
    public final String URL_UNIT_PROPERTY = "/um/property";
    public final String URL_UNIT_EDIT = "/um/edit";
    public final String URL_USER_NOTIFICATIONS = "/user/notifications";

    public void route(UI ui, String location) {
        ui.getPage().setLocation(location);
    }
}
