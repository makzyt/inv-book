package pl.makzyt.inventory_book.autorun;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.makzyt.inventory_book.connector.ServerConnector;
import pl.makzyt.inventory_book.model.*;
import pl.makzyt.inventory_book.repository.*;
import pl.makzyt.inventory_book.service.*;
import pl.makzyt.inventory_book.util.RouteUtils;

@Component
public class StartupEvent {
    private static String className = StartupEvent.class.getName();
    private static Logger logger = Logger.getLogger(className);
    private ServerConnector connector = new ServerConnector();

    @Autowired
    private CellService cellService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ITSystemService itSystemService;

    @Autowired
    private CellRepository cellRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private ITSystemRepository itSystemRepository;

    @EventListener(ContextRefreshedEvent.class)
    public void downloadRepositories() {
        loadCellRepository();
        loadRoomRepository();
        loadEmployeeRepository();
        loadInventoryRepository();
        loadITSystemRepository();
    }

    private void loadCellRepository() {
        connector.setUrl(RouteUtils.EXT_URL_REST_CELL_ALL);

        JSONArray cells = new JSONArray(connector.responseFromHttpGet());

        for (int i = 0; i < cells.length(); i++) {
            JSONObject obj = cells.getJSONObject(i);
            Cell c = cellService.loadFromJson(obj);

            if (cellRepository.findById(c.getId()) == null) {
                cellRepository.save(c);
            }
        }

        cellRepository.flush();
        logger.info("Downloaded Cell repository");
    }

    private void loadRoomRepository() {
        connector.setUrl(RouteUtils.EXT_URL_REST_ROOM_ALL);

        JSONArray cells = new JSONArray(connector.responseFromHttpGet());

        for (int i = 0; i < cells.length(); i++) {
            JSONObject obj = cells.getJSONObject(i);
            Room r = roomService.loadFromJson(obj);

            if (roomRepository.findById(r.getId()) == null) {
                roomRepository.save(r);
            }
        }

        roomRepository.flush();
        logger.info("Downloaded Room repository");
    }

    private void loadEmployeeRepository() {
        connector.setUrl(RouteUtils.EXT_URL_REST_EMPLOYEE_ALL);

        JSONArray cells = new JSONArray(connector.responseFromHttpGet());

        for (int i = 0; i < cells.length(); i++) {
            JSONObject obj = cells.getJSONObject(i);
            Employee e = employeeService.loadFromJson(obj);

            if (employeeRepository.findById(e.getId()) == null) {
                employeeRepository.save(e);
            }
        }

        employeeRepository.flush();
        logger.info("Downloaded Employee repository");
    }

    private void loadInventoryRepository() {
        connector.setUrl(RouteUtils.EXT_URL_REST_INVENTORY_ALL);

        JSONArray cells = new JSONArray(connector.responseFromHttpGet());

        for (int i = 0; i < cells.length(); i++) {
            JSONObject obj = cells.getJSONObject(i);
            Inventory inv = inventoryService.loadFromJson(obj);

            if (inventoryRepository.findById(inv.getId()) == null) {
                inventoryRepository.save(inv);
            }
        }

        inventoryRepository.flush();
        logger.info("Downloaded Inventory repository");
    }

    private void loadITSystemRepository() {
        connector.setUrl(RouteUtils.EXT_URL_REST_ITSYSTEM_ALL);

        JSONArray cells = new JSONArray(connector.responseFromHttpGet());

        for (int i = 0; i < cells.length(); i++) {
            JSONObject obj = cells.getJSONObject(i);
            ITSystem its = itSystemService.loadFromJson(obj);

            if (itSystemRepository.findById(its.getId()) == null) {
                itSystemRepository.save(its);
            }
        }

        itSystemRepository.flush();
        logger.info("Downloaded IT System repository");
    }
}
