package pl.makzyt.inventory_book.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class URLUtilsTests {
    @Test
    public void testAddingParams() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("p1", "v1");
        params.put("p2", "v2");

        String url = "http://page.com/subpage";
        String urlWithParams = URLUtils.addParamToURL(url, params);

        Assert.assertEquals(url + "?p1=v1&p2=v2", urlWithParams);
    }

    @Test
    public void testAddingOneParam() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("p1", "v1");

        String url = "http://page.com/subpage";
        String urlWithParams = URLUtils.addParamToURL(url, params);

        Assert.assertEquals(url + "?p1=v1", urlWithParams);
    }
}
