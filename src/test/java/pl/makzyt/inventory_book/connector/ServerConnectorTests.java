package pl.makzyt.inventory_book.connector;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.makzyt.inventory_book.service.ERPService;
import pl.makzyt.inventory_book.util.RouteUtils;

import java.util.HashMap;

public class ServerConnectorTests {
    @Autowired
    private ERPService erpService;

    @Test
    public void testEmployeeArrayLength() {
        String url = RouteUtils.EXT_URL_REST_EMPLOYEE_ALL;
        ServerConnector connector = new ServerConnector(url);
        JSONArray array = new JSONArray(connector.responseFromHttpGet());

        Assert.assertEquals(9, array.length());
    }

    @Test
    public void testEmployeeObjectData() {
        String url = RouteUtils.EXT_URL_REST_EMPLOYEE_ALL;
        ServerConnector connector = new ServerConnector(url);
        JSONArray array = new JSONArray(connector.responseFromHttpGet());
        JSONObject object = array.getJSONObject(3);

        Assert.assertEquals("zenek", object.get("login"));
    }

    @Test
    public void testEmployeePostObjectResponse() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", "jkowalski");
        params.put("password", "jkowalski");

        String url = RouteUtils.EXT_URL_REST_CHECKLOGIN;
        ServerConnector connector = new ServerConnector(url);
        String response = connector.responseFromHttpPost(params, "");
        JSONObject object = new JSONObject(response);

        Assert.assertEquals(true, object.getBoolean("success"));
    }

    @Test
    public void testEmailPostObjectResponse() {
        String url = RouteUtils.EXT_URL_REST_EMAIL_SEND;
        ServerConnector connector = new ServerConnector(url);

        JSONObject obj = new JSONObject();
        obj.put("toaddress", "ADDRESS");
        obj.put("title", "TITLE");
        obj.put("content", "CONTENT");

        String s = connector.responseFromHttpPost(null, obj.toString());

        Assert.assertEquals("false", s);
    }
}
